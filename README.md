# Onlineliga User Script - Training Schedule Export and Import

<!-- TABLE OF CONTENTS -->
## Table of Contents

- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [User script installation](#user-script-installation)
- [Contributing](#contributing)
- [Development](#development)
- [Usage](#usage)

<!-- ABOUT THE PROJECT -->
## About The Project
This user script is meant to simplify the process of copying and pasting training schedules.

### Built With

* [Node.js](https://nodejs.org/)
* [Violentmonkey](https://violentmonkey.github.io/)
* [Docker](https://www.docker.com/)
* [vscode](https://code.visualstudio.com/)

<!-- GETTING STARTED -->
## Getting Started

The following steps will help you to get started with the script.

### Prerequisites

You will need a user script manager like:
* [Violentmonkey](https://violentmonkey.github.io/)
* [Tampermonkey](https://www.tampermonkey.net/)
* [Greasemonkey](https://www.greasespot.net/)

Follow the instructions on the website of the user script manager to install the manager.

___Note:___ Safari and Opera users... please...

### User script installation

#### Production
[Click here to install the production version][prod-install]. This will work out of the box with Violentmonkey and Greasemonkey.
For usage with Tampermonkey you will need to install the script explicitly.

1. Open the Tampermonkey dashboard
2. Go the `Utilities` tab
3. Click on `Install from URL` and paste [this link][prod-install].


<!-- CONTRIBUTING -->
## Contributing

You basically can do whatever you want. All you need to do is to create a pull request. CI/CD will lint and if everything is fine the pull request will be reviewed and merged.

<!-- DEVELOPMENT -->
## Development

For vscode users there is a `.devcontainer` folder which will help you to get started with the development environment.
You'll only need [Docker](https://www.docker.com/) installed on your machine and the [Dev Container Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) for vscode.

Once inside the container you can use the following commands to build and test the script.

```bash
# Install dependencies
yarn install
```

While developing you can use the following command to compile and watch for changes. The created script will be available in the `dist` folder.
You can drag this file into the browser and Violentmonkey can track changes made to the file and reload the script automatically.
This makes it really easy to develop the script.
```bash
# Compile and watch
yarn run dev
```

```bash
# Build script
yarn run build
```

```bash
# Lint script
yarn run lint
```

<!-- USAGE EXAMPLES -->
## Usage

The script will add a button to the training page. Clicking on the button will open a dialog where you can copy the training schedule.

The following example is a working training schedule. You can copy the json and paste it into the dialog to import the schedule.
``` json
{
  "schedule":{
    "name":"test",
    "active":true,
    "training_sets": [
      {
        "day": "monday",
        "staff": "TT",
        "staff_index": 0,
        "minutes": 30,
        "training_component_id": 11,
        "pitch_id": 1,
        "pitch_half" : 2,
        "training_group": "RENTNER"
      },
      {
        "day": "tuesday",
        "staff": "CT",
        "staff_index": 0,
        "minutes": 30,
        "training_component_id": 3,
        "pitch_id": 1,
        "pitch_half" : 2,
        "training_group": "DEFENSE"
      },
      {
        "day": "wednesday",
        "staff": "T",
        "staff_index": 0,
        "minutes": 30,
        "training_component_id": 6,
        "pitch_id": 0,
        "pitch_half" : 2,
        "training_group": "OFFENSE"
      }
    ]
  },
  "training_groups": [
    {
      "name": "RENTNER"
    },
    {
      "name": "DEFENSE"
    },
    {
      "name": "OFFENSE"
    }
  ]
}
```

<!-- MARKDOWN LINKS & IMAGES -->
[prod-install]: https://gitlab.com/onlineliga/user_scripts/-/jobs/artifacts/main/raw/dist/training-export-import.user.js?job=build
