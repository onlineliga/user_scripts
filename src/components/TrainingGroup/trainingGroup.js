// function to retrieve all training groups
export async function getTrainingGroups(token, scheduleId) {
  const queryParams = new URLSearchParams({
    trainingScheduleId: scheduleId,
    trainingGroupId: 0,
  });
  const res = await fetch('/team/training/group/edit?' + queryParams, {
    method: 'GET',
    headers: {
      'x-csrf-token': token,
    },
  });
  // return an error if the response is not 200
  if (!res.ok) {
    console.error(res);
    return;
  }
  const data = await res.text();
  // create a new DOM element to store the data
  const div = document.createElement('div');
  div.innerHTML = data;
  // find the first element with the class 'ol-dropdown-menu'
  const dropdown = div.querySelector('.ol-dropdown-menu');
  // find all elements with an attribute of data-trainingGroupId in data and store the values
  // and 'data-name' attribute in an array
  // sorted by the value of data-trainingGroupId
  // do not add duplicates
  const trainingGroups = Array.from(
    dropdown.querySelectorAll('[data-trainingGroupId]')
  )
    .map((el) => {
      return {
        id: el.getAttribute('data-trainingGroupId'),
        name: el.getAttribute('data-name'),
      };
    })
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort((a, b) => a.id - b.id);
  return trainingGroups;
}
