export default function ImportGroupsButton() {
  // Use JSX to create the element
  return VM.hm(
    'button',
    {
      class: 'ol-state-primary-color-13 ol-button-state',
      id: 'import-groups-button',
      style: 'margin-bottom: 10px; width: 100%; margin-top: 10px;',
      // Add an event listener to the button to handle importing of JSON data
      onclick: () => {
        // get the value of the element with an attribute of name="_token"
        const token = document.querySelector('input[name="_token"]').value;
        // log the value of the token
        console.log(token);
        // Get the JSON data from the textfield
        const data = JSON.parse(
          document.getElementById('import-textarea').value
        );
        // for every object in the training_groups array
        // create a new group and rename it accordingly to the name property
        data.training_groups.forEach((group) => {
          // Send a POST request to the server with the JSON data
          fetch('/team/training/group/add', {
            headers: {
              'content-type':
                'application/x-www-form-urlencoded; charset=UTF-8',
              'x-csrf-token': token,
            },
            body: 'trainingScheduleId=144493&trainingGroupId=267139&context=',
            method: 'POST',
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              // If the response is successful rename the group
              // the server will return a JSON object with a property of traingGroupId
              if (data.trainingGroupId) {
                // Get the name from the JSON data
                const name = group.name;
                // Send a POST request to the server with the new name
                fetch('/team/training/group/rename', {
                  headers: {
                    'content-type':
                      'application/x-www-form-urlencoded; charset=UTF-8',
                    'x-csrf-token': token,
                  },
                  body: `trainingGroupId=${data.trainingGroupId}&name=${name}`,
                  method: 'POST',
                })
                  .then((res) => res.json())
                  .then((data) => {
                    console.log(data);
                  });
              }
            });
        });
      },
    },
    'Import Training Groups'
  );
}
