// function to validate the input
// input is in JSON format
// containing training schedule and training groups
// returns false and an error message if the input is invalid
// example input:
// {
//   "schedule":{
//     "name":"test",
//     "active":true,
//     "training_sets": [
//       {
//         "day": "monday",
//         "staff": "CT",
//         "minutes": 30,
//         "training_component_id": 11,
//         "pitch_id": 1,
//         "pitch_half" : 2,
//         "training_group": "RENTNER"
//       },
//       {
//         "day": "tuesday",
//         "staff": "T",
//         "minutes": 30,
//         "training_component_id": 3,
//         "pitch_id": 1,
//         "pitch_half" : 2,
//         "training_group": "DEFENSE"
//       },
//       {
//         "day": "wednesday",
//         "staff": "T",
//         "minutes": 30,
//         "training_component_id": 6,
//         "pitch_id": 0,
//         "pitch_half" : 2,
//         "training_group": "OFFENSE"
//       }
//     ]
//   },
//   "training_groups": [
//     {
//       "name": "RENTNER"
//     },
//     {
//       "name": "DEFENSE"
//     },
//     {
//       "name": "OFFENSE"
//     }
//   ]
// }

export function validateInput(input) {
  // check if the input is empty
  if (input === undefined || input === null) {
    return [false, 'Input is empty'];
  }
  // check if the input is a string
  if (typeof input !== 'string') {
    return [false, 'Input is not a string'];
  }
  // check if the input is a valid JSON string
  try {
    input = JSON.parse(input);
  } catch (e) {
    return [false, 'Input is not a valid JSON string'];
  }
  // check if the input has a schedule property
  // do not use hasOwnProperty here, because schedule can be null
  if (input.schedule === undefined) {
    return [false, 'Input does not have a schedule property'];
  }
  // check if the input has a training_groups property
  // do not use hasOwnProperty here, because training_groups can be null
  if (input.training_groups === undefined) {
    return [false, 'Input does not have a training_groups property'];
  }
  // check if the schedule property is an object
  if (typeof input.schedule !== 'object') {
    return [false, 'Schedule is not an object'];
  }
  // check if the training_groups property is an array
  if (!Array.isArray(input.training_groups)) {
    return [false, 'Training groups is not an array'];
  }
  // check if the schedule property has a name property
  // do not use hasOwnProperty here, because name can be null
  if (input.schedule.name === undefined) {
    return [false, 'Schedule does not have a name property'];
  }
  // check if the schedule property has a training_sets property
  // do not use hasOwnProperty here, because training_sets can be null
  if (input.schedule.training_sets === undefined) {
    return [false, 'Schedule does not have a training_sets property'];
  }
  // check if the training_sets property is an array
  if (!Array.isArray(input.schedule.training_sets)) {
    return [false, 'Training sets is not an array'];
  }
  // check if the training_sets property is not empty
  if (input.schedule.training_sets.length === 0) {
    return [false, 'Training sets is empty'];
  }
  // check if the name property is a string
  if (typeof input.schedule.name !== 'string') {
    return [false, 'Name is not a string'];
  }
  // training_groups in training_sets must be in training_groups
  // loop through all training_sets
  for (let i = 0; i < input.schedule.training_sets.length; i++) {
    // check if the training_group property is a string
    if (typeof input.schedule.training_sets[i].training_group !== 'string') {
      return [false, 'Training group is not a string'];
    }
    // check if the training_group property is in training_groups
    if (
      !input.training_groups.some(
        (group) => group.name === input.schedule.training_sets[i].training_group
      )
    ) {
      return [false, 'Training group is not defined in training_groups'];
    }
  }
  // return true and a success message if the input is valid
  return [true, 'Input is valid'];
}
