export default function DeleteLineUpButton() {
  return VM.hm(
    'button',
    {
      className: 'ol-state-primary-color-13 ol-button-state',
      id: 'delete-lineup-button',
      style: 'margin-bottom: 10px; width: 100%; margin-top: 10px;',
      // async function to handle the click event
      onclick: async () => {
        const token = document.querySelector('input[name="_token"]').value;
        // get the lineup ID
        const lineupId = getLineupId();
        // print the lineup ID to the console
        console.log(lineupId);
        // get the lineup data
        const lineup = getLineupById(lineupId);
        // print the lineup data to the console
        console.log(lineup);
        // parse the player IDs from the lineup
        const playerIds = parsePlayerIds(lineup);
        // print the player IDs to the console
        console.log(playerIds);
        // get the formation ID
        const formationId = getFormationIdByLineupId(lineupId);
        // print the formation to the console
        console.log(formationId);
        // remove all players from the lineup
        await removeAllPlayers(token, playerIds, lineupId, formationId);
        // get lineup element
        const refreshButton = getRefreshButtonById(lineupId);
        // print the lineup element to the console
        console.log(refreshButton);
        // click the lineup element to refresh the page
        refreshButton.click();
      },
    },
    'Delete Lineup'
  );
}

// function to remove all players from the lineup and bench
async function removeAllPlayers(token, playerIds, lineupId, formationId) {
  // a lineup consists of 11 players and 7 substitutes
  // players that are not in the lineup are called "not in the squad"
  // the maximum number of players in the squad is 18
  // the starting number of players outside the squad is 19
  // the request to be sent to the server is a POST request to the following URL:
  // /team/lineup/update
  // the request body looks like this:
  // userLineUpId=109054&player%5B4%5D%5BplayerId%5D=1782408&player%5B4%5D%5Bindex%5D=4&player%5B8%5D%5BplayerId%5D=1527427
  // map all player ids to a number higher than 18 increasing by 1
  // this will move all players to the "not in the squad" section
  const mappedPlayerIds = playerIds.map((playerId, index) => {
    return {
      playerId,
      index: index + 19,
    };
  });
  // create a serchParams object
  const searchParams = new URLSearchParams();
  // append the formation ID to the searchParams object
  searchParams.append('systemId', formationId);
  // append the lineup ID to the searchParams object
  searchParams.append('userLineUpId', lineupId);
  // loop through the mapped player IDs
  mappedPlayerIds.forEach((player) => {
    // append the player ID to the searchParams object
    searchParams.append(`player[${player.index}][playerId]`, player.playerId);
    // append the index to the searchParams object
    searchParams.append(`player[${player.index}][index]`, player.index);
  });
  // send the request to the server
  const response = await fetch('/team/lineup/update?', {
    method: 'POST',
    headers: {
      'X-CSRF-TOKEN': token,
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: searchParams,
  });
  // return the response
  return response;
}

// function to parse all player IDs from the lineup and bench
// returns an array of player IDs
function parsePlayerIds(lineup) {
  // line up is a JSON object
  // this objects looks like this:
  // {"1879847":1,"1848199":2,"1907789":3,"1950839":4,"1976249":5,"1597111":6,"1598900":7,"1375942":8,"1599583":9,"1378042":10,"1680644":11,"1679492":12,"1782408":13,"1428763":14,"1688167":15,"1910199":16,"2061161":17,"1679299":18,"1527427":19,"1378587":20,"2061504":21,"2092777":22,"2084132":23,"2115644":24,"1302279":25,"1846648":26}
  // parse the JSON object
  const parsedLineup = JSON.parse(lineup);
  // create an array to hold the player IDs
  const playerIds = [];
  // loop through the parsed lineup
  for (const playerId in parsedLineup) {
    // push the player ID to the array
    playerIds.push(playerId);
  }
  // return the array of player IDs
  return playerIds;
}

// function to get current lineup ID
// returns the lineup ID
function getLineupId() {
  // find the element with the id 'dropdownLineUps'
  const currentLineup = document.getElementById('dropdownLineUps');
  // get the value of the attribute of data-value
  const lineupId = currentLineup.getAttribute('data-value');
  // return the lineup ID
  return lineupId;
}

// function to get lineup by ID
// returns a JSON object with the lineup data
function getLineupById(lineupId) {
  // find the element with the attribute of data-value with the value of lineupId
  // and which has the id of liLineUp concatenated with the lineupId
  const lineup = document.querySelector(
    `[data-value="${lineupId}"][id="liLineUp${lineupId}"]`
  );
  // get the value of the attribute of data-player-positions
  const lineupData = lineup.getAttribute('data-player-positions');
  // return the lineup data
  return lineupData;
}

// function get formation ID by lineup ID
// returns the formation ID
function getFormationIdByLineupId(lineupId) {
  // find the element with the attribute of data-value with the value of lineupId
  // and which has the id of liLineUp concatenated with the lineupId
  const lineup = document.querySelector(
    `[data-value="${lineupId}"][id="liLineUp${lineupId}"]`
  );
  // get the value of the attribute of data-team
  const formationJSON = lineup.getAttribute('data-team');
  // this is a JSON object and we need to parse the formationId out of it
  // the formation ID is the value of the key "formationId"
  // parse the JSON object
  const parsedFormationId = JSON.parse(formationJSON);
  // get the formation ID
  const formationId = parsedFormationId.formationId;
  // return the formation ID
  return formationId;
}

// function to find the refresh button by Id which looks like this:
// <a onclick="olTeamSettings.onClickDropdownUserLineUp(148651)">Neue Aufstellung (1)</a>
// returns the refresh button
function getRefreshButtonById(lineupId) {
  // find the element of tag a with the attribute of onclick with the value of olTeamSettings.onClickDropdownUserLineUp concatenated with the lineupId
  const refreshButton = document.querySelector(
    `a[onclick="olTeamSettings.onClickDropdownUserLineUp(${lineupId})"]`
  );
  // return the refresh button
  return refreshButton;
}
