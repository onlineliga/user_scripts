export default function ImportTextArea() {
  return VM.hm('textarea', {
    className: 'ol-support-msg-textarea',
    id: 'import-textarea',
    rows: '10',
    placeholder: `{"training_groups":[{"name":"Group 1"},{"name":"Group 2"},{"name":"Group 3"}]}`,
    style: 'width: 100%; font-family: monospace;',
  });
}
