import { getTrainingStaff } from './trainingStaff';
import { canCreateSchedule } from './trainingSchedule';
import { getInfrastructures } from './trainingInfrastructure';
import { createSchedule } from './trainingSchedule';
import { validateInput } from '../Validate/inputValidation';
import { getTrainingGroups } from '../TrainingGroup/trainingGroup';
import { createSession, editSession } from './trainingSession';

export default function ImportScheduleButton() {
  return VM.hm(
    'button',
    {
      className: 'ol-state-primary-color-13 ol-button-state',
      id: 'import-schedule-button',
      style: 'margin-bottom: 10px; width: 100%; margin-top: 10px;',
      // Add an event listener to the button to handle importing of JSON data
      // async function to handle the click event
      onclick: async () => {
        // get schedule from textfield
        const importScheduleText =
          document.getElementById('import-textarea').value;
        // validate schedule
        const valid = validateInput(importScheduleText);
        // if the data is not valid log an error and return e.g [false, 'error message']
        if (!valid[0]) {
          console.error(valid[1]);
          return;
        }
        const token = document.querySelector('input[name="_token"]').value;
        if (canCreateSchedule(token)) {
          console.log('can create schedule');
          // get training staff
          const staff = await getTrainingStaff(token);
          console.log(staff);

          // create a new training schedule and get the id
          const scheduleId = await createSchedule(token);
          // get infrastructures
          const infrastructures = await getInfrastructures(token, scheduleId);
          console.log(infrastructures);
          // get training groups
          const trainingGroups = await getTrainingGroups(token);
          console.log(trainingGroups);
          // parse schedule
          const importSchedule = JSON.parse(importScheduleText);
          console.log(importSchedule);
          // iterate over training sets in schedule and create training sessions
          for (let trainingSet of importSchedule.schedule.training_sets) {
            // get training group id by the name of the training group in the import
            const trainingGroupId = trainingGroups.find(
              (group) => group.name === trainingSet.training_group
            ).id;
            // get staff id by the staff and staff_index in the import
            const staffId = staff[trainingSet.staff][trainingSet.staff_index];
            // get infrastructure id by the name of the infrastructure in the import
            // infrastructureId is the item in the infrastructures array of pitch_id
            const infrastructureId = infrastructures[trainingSet.pitch_id];
            // get day
            const day = trainingSet.day;
            // get row
            const row = trainingSet.row;
            // get pitch half
            const pitchHalf = trainingSet.pitch_half;
            // get minutes
            const minutes = trainingSet.minutes;

            // create training session
            const trainingSetId = await createSession(
              token,
              scheduleId,
              trainingGroupId,
              infrastructureId,
              row,
              day,
              staffId,
              pitchHalf,
              minutes
            );

            // edit training session
            await editSession(
              token,
              trainingSetId,
              trainingSet.training_component_id,
              -1,
              -1,
              minutes
            );
          }
        }
      },
    },
    'Import Schedule'
  );
}
