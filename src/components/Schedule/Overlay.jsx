import ExportScheduleButton from './ExportScheduleButton';
import ImportScheduleButton from './ImportScheduleButton';
import ImportTextArea from '../ImportTextArea';

export function DialogButton() {
  return VM.hm(
    'button',
    {
      className: 'ol-state-primary-color-13 ol-button-state',
      id: 'export-schedule-button',
      style: 'margin-bottom: 10px; width: 100%; margin-top: 10px;',
      // async function to handle the click event
      onclick: async () => {
        // show the dialog box
        showDialog();
      },
    },
    'Dialog'
  );
}

// Create a function to show the dialog box
function showDialog() {
  // Create the dialog box element
  const dialog = VM.hm(
    'div',
    {
      id: 'schedule-dialog',
      className: 'ol-overlay-window scroll-partsize',
      style:
        'display: block; position: fixed; width: 100%; height: 100%; z-index: 9999;',
    },
    VM.hm(
      'div',
      {
        className: 'ol-overlay-window-header',
      },
      VM.hm('span', {
        className:
          'ol-overlay-window-header-title ol-tile-overlay-window-headline',
        textContent: 'IMPORT EXPORT EDITOR',
      }),
      VM.hm(
        'a',
        {
          className: 'ol-overlay-window-header-close',
        },
        VM.hm('div', {
          className: 'ol-overlay-window-header-close icon-close_button',
          onclick: () => {
            document.getElementById('schedule-dialog').remove();
          },
        })
      )
    ),
    VM.hm(
      'div',
      {
        className: 'ol-overlay-window-content',
      },
      // add a view to the dialog box
      VM.hm(
        'div',
        {
          id: 'schedule-dialog-view',
          className: 'ol-transfer-list-bid-view',
        },
        // add the import and export buttons to the view
        VM.hm(ExportScheduleButton),
        VM.hm(ImportScheduleButton),
        // add the import text area to the view
        VM.hm(ImportTextArea)
      )
    )
  );

  // Add the dialog box to the page
  document.body.appendChild(dialog);
}
