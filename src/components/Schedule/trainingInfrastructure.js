// function to retrieve all training pitches (infrastructures)
export async function getInfrastructures(token, scheduleId) {
  const queryParams = new URLSearchParams({
    trainingScheduleId: scheduleId,
    day: 'monday',
  });
  const res = await fetch('/team/training/timeline/day?' + queryParams, {
    method: 'GET',
    headers: {
      'x-csrf-token': token,
    },
  });
  // return an error if the response is not 200
  if (!res.ok) {
    console.error(res);
    return;
  }
  const data = await res.text();
  // create a new DOM element to store the data
  const div = document.createElement('div');
  div.innerHTML = data;
  // find all elements with an attribute of data-infrastructureId in data and store the values in an array
  // sorted by the value of data-infrastructureId
  // do not add duplicates
  const infrastructures = Array.from(
    div.querySelectorAll('[data-infrastructureId]')
  )
    .map((el) => el.getAttribute('data-infrastructureId'))
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort();
  return infrastructures;
}

// function get infrastucture index
// returns the index of the infrastructure in the array of infrastructures
export function getInfrastructureIndex(infrastructureId, infrastructures) {
  return infrastructures.indexOf(infrastructureId);
}

// extract an array of infrastructure ids sorted by infrastructure id of all training sets in a schedule
export function extractInfrastructure(schedule) {
  return schedule.trainingSets
    .map((trainingSet) => trainingSet.infrastructureId)
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort();
}
