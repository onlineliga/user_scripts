import Spinner from '../Spinner';

// function to create a training session
export async function createSession(
  token,
  scheduleId,
  trainingGroupId,
  infrastructureId,
  row,
  day,
  staffId,
  pitchHalf,
  minutes
) {
  // Set InfrastructureId to 0 if it is undefined / empty
  if (!infrastructureId) {
    infrastructureId = 0;
  }
  // Send a POST request to create a new training session
  // towards /team/training/slot/add
  const res = await fetch('/team/training/slot/add?', {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'x-csrf-token': token,
    },
    body: new URLSearchParams({
      trainingScheduleId: scheduleId,
      trainingGroupId: trainingGroupId,
      day: day,
      row: row,
      infrastructureId: infrastructureId,
      half: pitchHalf,
      staffId: staffId,
      minutes: minutes,
    }),
  });
  // return an error if the response is not HTTP 200
  if (!res.ok) {
    console.error(res);
    return;
  }
  const data = await res.json();
  // return an error if data.err is not empty
  if (data.err) {
    console.error(data.err);
  }
  // return trainingSetId
  return data.trainingSetId;
}

// function to edit a training session
export async function editSession(
  token,
  trainingSetId,
  trainingComponentId,
  staffId,
  trainingGroupId,
  minutes
) {
  // Send a POST request to edit a training session
  // towards /team/training/slot/save
  const res = await fetch('/team/training/slot/save?', {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'x-csrf-token': token,
    },
    body: new URLSearchParams({
      trainingSetId: trainingSetId,
      trainingComponentId: trainingComponentId,
      staffId: staffId,
      trainingGroupId: trainingGroupId,
      minutes: minutes,
    }),
  });
  // return an error if the response is not HTTP 200
  if (!res.ok || res.status !== 200) {
    console.error(res);
    return;
  }
  return;
}

// function to list a training session
export async function listSession(token, trainingSetId) {
  // Send a GET request to list a training session
  // towards /team/training/slot/list
  const res = await fetch(
    `/team/training/slot/edit?trainingSetId=${trainingSetId}`,
    {
      method: 'GET',
      headers: {
        'x-csrf-token': token,
      },
    }
  );
  // return an error if the response is not HTTP 200
  if (!res.ok || res.status !== 200) {
    console.error(res);
    return;
  }
  const data = await res.text();
  // return the data
  return data;
}

// export an array of week days
export const weekDays = [
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
  'sunday',
];

// function to retrieve all training sessions for all days
// returns an array of training sessions
export async function getWeekTrainingSets(token, scheduleId) {
  // add spinner
  const spinner = Spinner();
  // add spinner to the overlay with id 'schedule-dialog'
  document.getElementById('schedule-dialog').appendChild(spinner);
  // create an empty array
  const trainingSets = [];
  // loop through all days
  for (const day of weekDays) {
    // retrieve all training sessions for a specific day
    const dayTrainingSets = await getTrainingSets(token, scheduleId, day);
    // add the training sessions to the array
    trainingSets.push(...dayTrainingSets);
    // add message
    const message = `Day ${day}: ${dayTrainingSets.length} training sets retrieved`;
    // update the spinner message
    document.getElementById('spinner-message').textContent = message;
  }
  // remove spinner
  spinner.remove();
  // return the array
  return trainingSets;
}

// function to retrieve all training sets for a specific day and schedule
// returns an array of training sets
export async function getTrainingSets(token, scheduleId, day) {
  // create the query parameters
  const queryParams = new URLSearchParams({
    trainingScheduleId: scheduleId,
    day: day,
  });
  // send the request
  const res = await fetch('/team/training/timeline/day?' + queryParams, {
    method: 'GET',
    headers: {
      'x-csrf-token': token,
    },
  });
  // return an error if the response is not 200
  if (!res.ok) {
    console.error(res);
    return;
  }
  const data = await res.text();
  // create a new DOM element to store the data
  const div = document.createElement('div');
  div.innerHTML = data;
  // find all elements with an attribute of data-trainingSetId and class where class is ol-training-timetable-block-wrapper in data and store the values in an array
  // extracting the values of data-day, data-row, data-infrastructureId, data-half, data-staffId, data-trainingSetId, data-minutes
  const trainingSets = Array.from(
    div.querySelectorAll(
      '[data-trainingSetId].ol-training-timetable-block-wrapper'
    )
  ).map((el) => {
    return {
      day: el.getAttribute('data-day'),
      row: el.getAttribute('data-row'),
      infrastructureId: el.getAttribute('data-infrastructureId'),
      // pitchIndex is going to be computed later
      pitchIndex: undefined,
      half: el.getAttribute('data-half'),
      staffId: el.getAttribute('data-staffId'),
      staffType: extractStaffType(el),
      // staffIndex is going to be computed later
      staffIndex: undefined,
      trainingSetId: el.getAttribute('data-trainingSetId'),
      minutes: el.getAttribute('data-minutes'),
      trainingComponentId: extractTrainingComponentId(el),
      trainingGroupName: extractTrainingGroupName(el),
    };
  });
  // return the array
  return trainingSets;
}

// TODO: trainingGroupIds memo: prefix them

// create a map of training components
const trainingComponents = new Map([
  ['icon-icon_kondition', 1],
  ['icon-icon_regeneration', 2],
  ['icon-icon_koordination', 3],
  ['icon-icon_standards', 4],
  ['icon-icon_taktik', 5],
  ['icon-icon_schnellkraft', 6],
  ['icon-icon_trainingsspiel', 7],
  ['icon-icon_technik', 8],
  ['icon-icon_schusstraining', 9],
  ['icon-icon_stabilisation', 10],
  ['icon-icon_torwart', 11],
  ['icon-icon_spielformen4-4', 12],
  ['icon-icon_spielformen10-10', 13],
]);

// find the element with the class ol-training-timetable-block-icon and only return the first class attribute
// <div class="icon-icon_spielformen10-10 ol-training-timetable-block-icon"></div>
// and map the class attribute to the trainingComponentId
// trainingComponentId: trainingComponents.get(
//   el.querySelector('.ol-training-timetable-block-icon').classList[0]
// ),
function extractTrainingComponentId(el) {
  return trainingComponents.get(
    el
      .querySelector('.ol-training-timetable-block-icon')
      // iterate over the classes and return the first class that exists in the trainingComponents map
      .classList.value.split(' ')
      .find((className) => trainingComponents.has(className))
  );
}

// function to extract the staff type which is stored in the text of
// a child element with the class 'ol-training-timetable-block-staff-type'
function extractStaffType(el) {
  let staffType = el.querySelector(
    '.ol-training-timetable-block-staff-type'
  ).innerText;
  // strip whitespaces if any
  staffType = staffType.replace(/\s/g, '');
  // if the staff type is 'GT' (english translation for goal keeper trainer) change it to 'TT' (torwarttrainer)
  if (staffType === 'GT') {
    staffType = 'TT';
  }
  // return the staff type
  return staffType;
}

// function to extract the training group name which is stored in the text of
// a child element with the class 'ol-training-timetable-block-text-name'
function extractTrainingGroupName(el) {
  return el.querySelector('.ol-training-timetable-block-text-name').innerText;
}

// function to determine a staff index by staff type, staff id and staff array
// returns a number
export function getStaffIndex(staffType, staffId, staff) {
  // get the staff array for the staff type
  const staffTypeArray = staff[staffType];
  // find the index of the staff id in the staff array
  const staffIndex = staffTypeArray.findIndex((staff) => staff.id === staffId);
  // return the staff index
  return staffIndex;
}

// function to extract staff from training sets
// returns an object with staff types as keys and staff arrays as values
export function extractStaff(trainingSets) {
  // create an empty object
  const staff = {};
  // loop through all training sets
  for (const trainingSet of trainingSets) {
    // get the staff type
    const staffType = trainingSet.staffType;
    // get the staff id
    const staffId = trainingSet.staffId;
    // if the staff type is not in the staff object create an empty array
    if (!staff[staffType]) {
      staff[staffType] = [];
    }
    // if the staff id is not in the staff array add it
    if (!staff[staffType].find((staff) => staff.id === staffId)) {
      staff[staffType].push({
        id: staffId,
      });
    }
  }
  // return the staff object
  return staff;
}

// function to extract all training groups from all training sets
// returns an array of training groups
export function extractTrainingGroups(trainingSets) {
  // extract an array of training groups sorted by training group name of all training sets
  const trainingGroups = trainingSets
    .map((trainingSet) => trainingSet.trainingGroupName)
    .filter((value, index, self) => self.indexOf(value) === index)
    .sort();
  // return the training groups
  return trainingGroups;
}
