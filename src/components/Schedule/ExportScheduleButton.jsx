import { getSchedule, scheduleToJSON } from './trainingSchedule';

export default function ExportScheduleButton() {
  return VM.hm(
    'button',
    {
      className: 'ol-state-primary-color-13 ol-button-state',
      id: 'export-schedule-button',
      style: 'margin-bottom: 10px; width: 100%; margin-top: 10px;',
      // async function to handle the click event
      onclick: async () => {
        const token = document.querySelector('input[name="_token"]').value;
        const schedule = await getSchedule(token);
        // log the schedule to the console
        console.log(schedule);
        // convert the schedule to JSON
        const json = scheduleToJSON(schedule);
        // send JSON to the clipboard
        navigator.clipboard.writeText(json);
        // make the JSON available for download
        downloadSchedule(json, schedule);
      },
    },
    'Export Schedule'
  );
}

function downloadSchedule(json, schedule) {
  const blob = new Blob([json], { type: 'application/json' });
  const url = URL.createObjectURL(blob);
  const a = document.createElement('a');
  // set the download attribute with schedule id as prefix
  a.setAttribute('download', `${schedule.id}-schedule.json`);
  // set the href attribute
  a.setAttribute('href', url);
  // click the link
  a.click();
}
