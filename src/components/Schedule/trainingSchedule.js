import {
  getWeekTrainingSets,
  extractStaff,
  getStaffIndex,
  extractTrainingGroups,
} from './trainingSession.js';
import {
  getInfrastructureIndex,
  extractInfrastructure,
} from './trainingInfrastructure.js';

// function to create a training schedule
// returns the schedule id
export async function createSchedule(token) {
  const res = await fetch('/team/training/schedule/add', {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'x-csrf-token': token,
    },
  });
  const data = await res.json();
  // return an error if data.err is not empty
  if (data.err) {
    console.error(data.err);
    return 0;
  }
  // convert the data to an HTML element
  const dataElement = document.createElement('div');
  dataElement.innerHTML = data.html;
  // find all elements with an attribute of data-value and store the values in an array
  const values = Array.from(dataElement.querySelectorAll('[data-value]')).map(
    (el) => el.getAttribute('data-value')
  );
  // find the highest value in the array which is the id of the new schedule
  const max = Math.max(...values);
  // return the id
  console.log(`Created schedule with id ${max}`);
  return max;
}

// function to determine whether a schedule can be created
// returns true if a schedule can be created
export function canCreateSchedule() {
  // find the button element with the id 'btnScheduleAdd'
  const btn = document.getElementById('btnScheduleAdd');
  // check if the button has the attribute 'disabled'
  if (btn.hasAttribute('disabled')) {
    // log an error
    console.error(
      'Cannot create schedule, either premium acccount is required or the maximum number of schedules has been reached'
    );
    return false;
  }
  return true;
}

// function to remove a training schedule
// returns true if the schedule was removed
export async function removeSchedule(token, scheduleId) {
  // create the form data
  const formData = new FormData();
  formData.append('trainingScheduleId', scheduleId);
  // send the request
  const res = await fetch('/team/training/schedule/delete', {
    method: 'POST',
    headers: {
      'x-csrf-token': token,
    },
    body: formData,
  });
  const data = await res.json();
  // return an error if data.err is not empty
  if (data.err) {
    console.error(data.err);
    return false;
  }
  // log success
  console.log(`Removed schedule with id ${scheduleId}`);
  return true;
}

// function to receive current training schedule
// returns the schedule id
export async function getScheduleId() {
  // find the select element with the id 'dropdownTrainingSchedule'
  const select = document.getElementById('dropdownTrainingSchedule');
  // return the value of the attribute 'data-value' if attribute exists including a null check
  return select?.getAttribute('data-value') ?? 0;
}

// function to receive current training schedule
// returns an object with the schedule id and name and an array of training sessions
export async function getSchedule(token) {
  const schedule = {};
  const scheduleId = await getScheduleId();
  // return an error if scheduleId is 0
  if (scheduleId === 0) {
    console.error('Cannot get schedule, no schedule selected');
    return { id: 0, name: '', sessions: [] };
  }
  schedule.id = scheduleId;
  // prefix the schedule name with the schedule id and a dash
  schedule.name = `${scheduleId}-test`;
  // get the training sessions
  schedule.trainingSets = await getWeekTrainingSets(token, scheduleId);
  // get staff members assigned to training sets
  const staff = extractStaff(schedule.trainingSets);
  // log the staff members
  console.log(staff);
  // iterate over training sets and add staff index attribute to each training set
  schedule.trainingSets.forEach((trainingSet) => {
    trainingSet.staffIndex = getStaffIndex(
      trainingSet.staffType,
      trainingSet.staffId,
      staff
    );
  });

  // extract an array of infrastructure ids sorted by infrastructure id of all training sets
  const infrastructures = extractInfrastructure(schedule);
  // log the infrastructures
  console.log(infrastructures);

  // iterate over training sets and add infrastructure index attribute to each training set
  schedule.trainingSets.forEach((trainingSet) => {
    trainingSet.pitchIndex = getInfrastructureIndex(
      trainingSet.infrastructureId,
      infrastructures
    );
  });

  // extract training groups from training sets
  schedule.trainingGroups = extractTrainingGroups(schedule.trainingSets);

  // return the schedule
  return schedule;
}

// function to convert the schedule to a JSON string
// returns the JSON string
export function scheduleToJSON(schedule) {
  // create the JSON object
  const json = {
    schedule: {
      name: schedule.name,
      training_sets: [],
    },
    training_groups: [],
  };
  // iterate over training sets and add them to the JSON object
  schedule.trainingSets.forEach((trainingSet) => {
    json.schedule.training_sets.push({
      day: trainingSet.day,
      staff: trainingSet.staffType,
      staff_index: trainingSet.staffIndex,
      minutes: trainingSet.minutes,
      training_component_id: trainingSet.trainingComponentId,
      row: trainingSet.row,
      pitch_id: trainingSet.pitchIndex,
      pitch_half: trainingSet.half,
      training_group: trainingSet.trainingGroupName,
    });
  });
  // iterate over training groups and add them to the JSON object
  schedule.trainingGroups.forEach((trainingGroup) => {
    json.training_groups.push({
      name: trainingGroup,
    });
  });
  // return the JSON string
  return JSON.stringify(json);
}
