import { createSchedule, removeSchedule } from './trainingSchedule';
import { createSession, editSession, listSession } from './trainingSession';

// function to get the training staff of your team
// returns an array of objects
export async function getTrainingStaff(token) {
  // we need to create a training schedule first
  // this is because the training staff is only available after a schedule has been created
  // and a training set has to be created for the schedule
  // one training set for goalkeeper coaches and one for the rest
  const scheduleId = await createSchedule(token);
  // create the training sets
  const [trainingSetIdGK, trainingSetIdRest] = await createMockTrainingSessions(
    token,
    scheduleId
  );
  // list the training sessions for both training sets
  const trainingSessionGKText = await listSession(token, trainingSetIdGK);
  const trainingSessionRestText = await listSession(token, trainingSetIdRest);
  // Convert the text to a DOM element
  const trainingSessionGK = document.createElement('div');
  trainingSessionGK.innerHTML = trainingSessionGKText;
  const trainingSessionRest = document.createElement('div');
  trainingSessionRest.innerHTML = trainingSessionRestText;
  // for both training sessions find following element <a onclick="olTraining.onSelectEditSlotStaff(event)
  // and get the staff id from the data-value attribute
  // and the staff type from the data-type attribute from the text of the first child element
  // and store the staff id and staff type in an array
  const staffGK = Array.from(
    trainingSessionGK.querySelectorAll(
      'a[onclick="olTraining.onSelectEditSlotStaff(event)"]'
    )
  ).map((el) => {
    return {
      staffId: el.getAttribute('data-value'),
      staffType: el.children[0].textContent,
    };
  });
  const staffRest = Array.from(
    trainingSessionRest.querySelectorAll(
      'a[onclick="olTraining.onSelectEditSlotStaff(event)"]'
    )
  ).map((el) => {
    return {
      staffId: el.getAttribute('data-value'),
      // if the text is 'GT' then it is a goalkeeper coach and safe it as 'TT'
      staffType:
        el.children[0].textContent === 'GT' ? 'TT' : el.children[0].textContent,
    };
  });

  // merge the staff for the goalkeeper coaches and the rest of the staff
  const staff = [...staffGK, ...staffRest];
  // create object with staff for each staff type
  const staffByType = {};
  for (const s of staff) {
    if (staffByType[s.staffType]) {
      staffByType[s.staffType].push(s.staffId);
    } else {
      staffByType[s.staffType] = [s.staffId];
    }
  }

  // remove the training schedule
  await removeSchedule(token, scheduleId);
  // return the staff by type
  return staffByType;
}

// function to create mock training sessions
// returns an the training set id for coach training and the training set id for staff training
export async function createMockTrainingSessions(token, scheduleId) {
  // create the training set for the goalkeeper coaches
  const trainingGroupId = 0;
  const day = 'monday';
  const row = 0;
  const staffId = 0;
  const pitchHalf = 1;
  const minutes = 30;
  const trainingSetIdGK = await createSession(
    token,
    scheduleId,
    trainingGroupId,
    0,
    row,
    day,
    staffId,
    pitchHalf,
    minutes
  );
  // edit the training session
  await editSession(token, trainingSetIdGK, 11, -1, -1, -1);
  // create the training set for the rest of the staff
  const trainingSetIdRest = await createSession(
    token,
    scheduleId,
    trainingGroupId,
    0,
    row,
    day,
    staffId,
    pitchHalf,
    minutes
  );

  return [trainingSetIdGK, trainingSetIdRest];
}
