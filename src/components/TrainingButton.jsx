export default function AdFreeTrainingButton() {
  // Use JSX to create the element
  const vdom = VM.h(
    'div',
    {
      class: 'start-training-row',
      style: 'margin-bottom: 10px;',
    },
    VM.h(
      'div',
      { class: 'col-xs-12' },
      VM.h(
        'div',
        {
          class:
            'ol-state-primary-color-13 ol-button-state ol-training-weektable-start-content training-action-whole-week column-width-8',
          id: 'start-training',
          // add click event that fetches the training data
          onclick: () => {
            fetch('/team/training/process')
              .then((res) => res.json())
              .then((data) => {
                console.log(data);
              });
          },
        },
        VM.h('div', { class: 'icon-icon_triangle_white' }),
        VM.h(
          'div',
          { class: 'ol-training-weektable-start-text' },
          'TRAIN WITHOUT ADS'
        )
      )
    )
  );
  // HTMLDivElement
  const el = VM.m(vdom);
  return el;
}
