export default function Spinner() {
  return VM.hm(
    'div',
    {
      id: 'overlay-spinner',
      style:
        'position: fixed; top: 0; left: 0; width: 100%; height: 100%; z-index: 9999; background-color: rgba(0, 0, 0, 0.5);',
    },
    VM.hm(
      'div',
      {
        className: 'message-container',
        style:
          'display: flex; align-items: center; justify-content: center; flex-direction: column; background-color: white; padding: 20px; border-radius: 5px; box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);',
      },
      VM.hm('p', {
        id: 'spinner-message',
        textContent: 'Loading...',
      }),
      VM.hm('div', {
        className: 'spinner',
        style:
          'border: 5px solid #f3f3f3; border-top: 5px solid #3498db; border-radius: 50%; width: 50px; height: 50px; animation: spin 2s linear infinite; margin-top: 20px;',
        // keyframes animation
        keyframes: {
          name: 'spin',
          from: {
            transform: 'rotate(0deg)',
          },
          to: {
            transform: 'rotate(360deg)',
          },
        },
      })
    )
  );
}
