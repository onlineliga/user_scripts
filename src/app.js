import DeleteLineUpButton from './components/LineUp/DeleteLineUpButton';
import { DialogButton } from './components/Schedule/Overlay';

const disconnect = VM.observe(document.body, () => {
  // Find the target node
  const node = document.querySelector('.start-training-row');

  // get current path
  const path = window.location.pathname;

  if (node && path === '/team/training') {
    const dialogButton = DialogButton();
    node.parentNode.insertBefore(dialogButton, node.nextSibling);
    return true;
  }

  const lineUpNode = document.querySelector('.ol-page-content');

  if (lineUpNode && path === '/team/lineup') {
    const deleteButton = DeleteLineUpButton();
    lineUpNode.parentNode.insertBefore(deleteButton, lineUpNode.nextSibling);

    return true;
  }
});

if (disconnect) {
  console.log('User script connected');
}
