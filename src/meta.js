// ==UserScript==
// @name        Training Export/Import
// @namespace   https://gitlab.com/onlineliga/user_scripts
// @description Adds functionality to export and import training schedule and groups
// @version     process.env.VERSION
// @author      process.env.AUTHOR
// @license     MIT
// @downloadURL https://gitlab.com/onlineliga/user_scripts/-/jobs/artifacts/main/raw/dist/training-export-import.user.js?job=build
// @updateURL   https://gitlab.com/onlineliga/user_scripts/-/jobs/artifacts/main/raw/dist/training-export-import.user.js?job=build
// @include     https://www.onlineliga.de/*
// @include     https://www.onlineliga.at/*
// @include     https://www.onlineliga.ch/*
// @include     https://www.onlineleague.co.uk/*
// @require     https://cdn.jsdelivr.net/combine/npm/@violentmonkey/dom@2,npm/@violentmonkey/ui@0.7
// ==/UserScript==

/**
 * Code here will be ignored on compilation. So it's a good place to leave messages to developers here.
 *
 * - The `@grant`s used in your source code will be added automatically by `rollup-plugin-userscript`.
 *   However you have to add explicitly those used in required resources.
 * - `process.env.VERSION` and `process.env.AUTHOR` will be loaded from `package.json`.
 */
